\contentsline {chapter}{List of Figures}{iii}{chapter*.4}
\contentsline {chapter}{List of Tables}{iv}{chapter*.5}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Literature Review}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Scope of the present research work}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Objectives of the research}{5}{section.2.2}
\contentsline {chapter}{\numberline {3}Methodology}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Path integral for bosons and Fermions}{6}{section.3.1}
\contentsline {paragraph}{ The path integral for fermions}{11}{section*.7}
\contentsline {section}{\numberline {3.2}Tetrads and connection dynamics}{13}{section.3.2}
\contentsline {paragraph}{Differential forms}{13}{section*.8}
\contentsline {paragraph}{}{14}{section*.9}
\contentsline {paragraph}{}{14}{section*.10}
\contentsline {paragraph}{Exterior derivative}{15}{section*.11}
\contentsline {paragraph}{Connection}{16}{section*.12}
\contentsline {paragraph}{Curvature}{17}{section*.13}
\contentsline {paragraph}{}{18}{section*.14}
\contentsline {paragraph}{covariant derivative}{18}{section*.15}
\contentsline {section}{\numberline {3.3}Hodge duality}{22}{section.3.3}
\contentsline {section}{\numberline {3.4}Topological action and invariance}{22}{section.3.4}
\contentsline {paragraph}{The Chern-Simons term}{23}{section*.16}
\contentsline {section}{\numberline {3.5}Lagrangians}{24}{section.3.5}
\contentsline {section}{\numberline {3.6}Gauge theory}{25}{section.3.6}
\contentsline {chapter}{\numberline {4}Results and Discussions}{26}{chapter.4}
\contentsline {section}{\numberline {4.1}Path integral for superconductors}{26}{section.4.1}
\contentsline {section}{\numberline {4.2}Dirac equation in curved space}{29}{section.4.2}
\contentsline {section}{\numberline {4.3}Holographic Action in tetrads}{31}{section.4.3}
\contentsline {chapter}{\numberline {5}Summary and conclusions}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}Summary of the work done}{37}{section.5.1}
\contentsline {section}{\numberline {5.2}Conclusions}{37}{section.5.2}
\contentsline {section}{\numberline {5.3}Work to be done}{38}{section.5.3}
\contentsline {section}{\numberline {5.4}Plan of research work}{39}{section.5.4}
\contentsline {chapter}{\numberline {6}Appendix}{40}{chapter.6}
\contentsline {paragraph}{Covariant derivative}{40}{section*.19}
\contentsline {paragraph}{}{40}{section*.20}
\contentsline {section}{\numberline {6.1}Spin connection Calculations}{41}{section.6.1}
\contentsline {section}{References}{44}{section.6.1}
\contentsline {chapter}{Bibliography}{45}{chapter*.21}
